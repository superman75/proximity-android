package com.example.lai.myproximity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Random;

import static android.widget.Button.OnClickListener;

/**
 * Created by Lai on 11/24/2017.
 * confirm window
 */

public class PopupMath extends AppCompatActivity {
    TextView mathView;
    EditText mathSolution;
    Button confirmButton;
    Button cancelButton;
    int a,b,c;
    boolean isConfirm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.popupmath);


        isConfirm  = false;

        // declare the confirm button, cancel button, math equation view and solution textView
        confirmButton = findViewById(R.id.ConfirmButton);
        cancelButton = findViewById(R.id.CancelButton);
        mathView = findViewById(R.id.Mathquestion);
        mathSolution = findViewById(R.id.Mathsolution);

        // display the math popup layout with proper size and proper position
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int)(width*0.7), (int)(height*0.5));

        // generate the random digit less than 10 for math equation and display the math equation
        final Random rand = new Random();
        a = rand.nextInt(10)+1;
        b = rand.nextInt(10)+1;
        mathView.setText(a+" X "+b+" = ");
        c = a*b;
        mathSolution.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            /*
            when the text content is changed, check if the result is correct or not
             */
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                int c1;
                try{
                    c1 = Integer.parseInt(String.valueOf(charSequence));
                } catch (Exception e){
                    c1 = 0;
                }
                if (c==c1) {
                    isConfirm = true;
                } else {
                    isConfirm = false;
                }


            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        /*
        when the confirm button is clicked,
        if the result is correct, transfer the RESULT_OK to the SensorActivity.
        Otherwise transfer the RESULT_CANCEL to the SensorActivity.
         */
        confirmButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                Intent intent = getIntent();
                if(isConfirm){
                    setResult(RESULT_OK,intent);
                } else {
                    setResult(RESULT_CANCELED,intent);
                }
                finish();
            }
        });
        /*
        when the cancel button is clicked, this activity is finished.
         */
        cancelButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }



}
