package com.example.lai.myproximity;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        int SPLASH_TIME_OUT = 0;

        /*
         here the activity is transform from the MainActivity to the SensorActivity
         */
        Intent intent = new Intent(getApplicationContext() ,SensorActivity.class);
        startActivity(intent);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                finish();

            }
        }, SPLASH_TIME_OUT);
    }
}
