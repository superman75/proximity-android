package com.example.lai.myproximity;

import android.app.Service;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;


/**
 * Created by Lai on 11/24/2017.
 * This class is created for Service.
 */

public class PopupMessage extends Service implements SensorEventListener{

    Sensor sensor;
    SensorManager sensorManager;
    Boolean isSensor;

    @Override
    public void onCreate(){
        super.onCreate();
        // proximity sensor and sensorManager is created.
        isSensor = false;
        sensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        assert sensorManager != null;
        sensor = sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_FASTEST);
    }
    @Override
    public IBinder onBind(Intent intent) {
        return null;

    }

    // when the service starts, the sensors are available

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        isSensor = true;
        return START_STICKY;

    }

    // when the service stops, the sensors are not available
    @Override
    public void onDestroy() {
        super.onDestroy();
        isSensor = false;

    }


    // if the proximity distance is less than 10cm, then show the alert message
    // otherwise send the broadcast to MyAlert Class to close the alert message
    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if(sensorEvent.sensor.getType() == Sensor.TYPE_PROXIMITY) {
            if (isSensor) {
                int max = sensor.getMaximumRange()>10?10: (int) sensor.getMaximumRange();
                if (sensorEvent.values[0] < max) {
                    Intent intent;
                    intent = new Intent(this, MyAlert.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                    startActivity(intent);
                }
                else {
                    sendBroadcast(new Intent("xyz"));
                }
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}
